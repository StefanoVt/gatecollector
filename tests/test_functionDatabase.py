from unittest import TestCase

import networkx as nx
from os.path import dirname
from pyrbc.graph import build_graph
from pyrbc.rbc import And, var

from gatecollector import database


class TestFunctionDatabase(TestCase):
    def test_addfunc(self):
        db = database.FunctionDatabase()
        db.addfunc(And(var("a"), var("b")), (var("a").node, var('b').node))


    def test_most_common(self):
        db = database.FunctionDatabase()
        db.addfunc(And(var("a"), var("b")), (var("a").node, var('b').node))
        print(db.most_common())

    def test_read_function(self):
        db = database.FunctionDatabase()
        with open(dirname(__file__)+ "/../resources/c3540.aig", "rb") as f:
            db.read_function(f, 4)
        print(db._count.most_common())
        for func in db.most_common():
            #g = build_graph([func])
            #nx.draw(g)
            print(func.inputs, func.outputs)
