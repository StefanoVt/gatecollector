#!/usr/bin/pypy
from glob import glob

import networkx as nx
from os.path import dirname
from pyrbc.graph import build_graph
from pyrbc.aiger import write_aig
from gatecollector import database
import sys
from datetime import datetime

sys.setrecursionlimit(100000)

def main():
    db = database.DelayedFunctionDatabase()

    for fn in  glob(dirname(__file__)+ "/../resources/c*.aig")[:]:

        print(datetime.now(), "processing ", fn)

        #read all gates from file
        try:
          with open(fn, "rb") as f:
            db.read_function(f, 6)
        except KeyboardInterrupt:
            if input("continue(Y/n)?") == "n":
                break
            else:
                continue
    # save to a file the most common gates
    for i,((size, func, inputs), count)  in enumerate(db.most_common(100)):
        print(i, size, count)

        with open(dirname(__file__)+ "/../resources/gates/g{}.aig".format(i), "wb") as f:
            write_aig(f, [func])

if __name__ == '__main__':
    main()