from pyrbc import rbc
from pyrbc.formula import Formula
from pyrbc.nodes.base import RBCEdge, RBCNode
from pyrbc.factory import RBCFactory
from typing import Tuple

def clone_func(output, inputs):
    # type: (RBCEdge, Tuple[RBCNode]) -> Formula
    "Clones a RBC cut into a separate function. Inputs will become var(0)..var(n) where n=len(inputs)"
    assert isinstance(output, RBCEdge)
    ret = clone_edge(output, inputs)
    newinps = tuple(rbc.var(i).node for i in range(len(inputs)))

    return Formula.build(ret, inputs=newinps)

def clone_edge(output, inputs):
    if output.node in inputs :
        ret = rbc.var(inputs.index(output.node))
        if output.flip:
            ret = ~ret
        return ret
    else:
        assert output.node.op != "var", "{} not in {}".format(output.node, inputs)
        rbcfactory = RBCFactory.singleton()
        newchildren = [clone_edge(x, inputs) for x in output.node.children]
        assert all(isinstance(x.node, RBCNode) for x in newchildren)
        ret = rbcfactory.new(output.node.__class__, *newchildren)
        if output.flip:
            ret = ~ret
        return ret


def func_size(output,inputs):
    # type: (RBCEdge, Tuple[RBCNode]) -> int
    if output.node in inputs:
        return 1
    else:
        return 1 + sum(func_size(x, inputs) for x  in output.node.children)