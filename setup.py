#!/usr/bin/env python2
from setuptools import find_packages, setup

setup(
    name='gatecollector',
    version='0.0.0',
    packages=find_packages(exclude=['tests']),
    install_requires=['canonicalize', 'kcuts', 'pyrbc'],
    test_suite='tests', )