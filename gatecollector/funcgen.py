from itertools import permutations, product
from canonicalize.canonicalize import FunctionCertificate
id = [ True, False]
zero = [False, False]

def permute(index, perm):
    return tuple(index[i] for i in perm)

def negate(index, negs):
    return [x!=y for x,y in zip(index, negs)]

def allpn(func, n):
    indices = list(product(*([(False,True)]*n)))
    funcdict = dict(zip(indices, func))
    for perm in permutations(range(n)):
        for negations in product(*([(False,True)]*n)):
            yield [funcdict[permute(negate(index,negations),perm)] for index in indices]



def all_funcs_of_size(prev_size, size):
    for first in prev_size:
        for second in prev_size:
            for firstpn in allpn(first, size -1):
                for secondpn in allpn(second, size -1):
                    yield firstpn + secondpn


def iter_all_funcs(maxsize):
    level = [id, zero]
    size = 1
    while size < maxsize:
        scraplevel = []
        for func in level:
            scrap = yield func
            if scrap:
                scraplevel.append(func)

        for scrapfunc in scraplevel:
            level.remove(scrapfunc)

        size += 1
        newlevel = dict()
        for newfunc in all_funcs_of_size(level, size):
            cert = FunctionCertificate(size, newfunc)
            cert.npncanonical()
            newlevel[cert] = newfunc
        level = newlevel.values()

    for func in level:
        yield func

def visit_all_funcs(maxsize, visitor):
    it = iter_all_funcs(maxsize)
    size = 1
    scrap = visitor(it.next(), size)
    doexit = False
    while not doexit:
        try:
            new = it.send(scrap)
            if len(new) > (1 << size):
                size += 1
                assert len(new) == 2 ** size
            scrap = visitor(new, size)
        except StopIteration:
            doexit = True




