from collections import Counter
from itertools import product

import networkx as nx
from canonicalize.canonicalize import FunctionCertificate
from pyrbc.fasttruthtable import truth_table as fasttrutht
from kcuts.kcuts import all_cuts2
from pyrbc.aiger import parse_aig
from pyrbc.formula import Formula
from pyrbc.graph import build_graph
from pyrbc.nodes.base import RBCNode, RBCEdge
from typing import Any, List, Tuple, Optional, Dict, BinaryIO

from .rbccuts import func_size, clone_func

#def productdict(inps):
#    for asgn in product(*([(0,1)]*len(inps))):
#        yield dict(zip(inps, asgn))


def docert(func, inputs):
    return FunctionCertificate(len(inputs), func_eval_cached(func,inputs))


_clone_cache = dict()
clone_hit = 0
def clone_func_cached(func, inputs):
    global  _clone_cache, clone_hit
    if (func,inputs) not in _clone_cache:
        nfunc =  clone_func(func, inputs)
        _clone_cache[func, inputs] = nfunc
        #assert fasttrutht(func,inputs) == fasttrutht(nfunc, newinputs)
        #inputs = newinputs
        #func = nfunc
    else:
        clone_hit += 1
    return _clone_cache[func, inputs]#, inputs

_eval_cache = dict()
eval_hit = 0
def func_eval_cached(func, inputs):
    global  _eval_cache, eval_hit
    if (func,inputs) not in _eval_cache:
        _eval_cache[func,inputs] = tuple(map(int, fasttrutht(func,inputs)))
    else:
        eval_hit += 1
    return _eval_cache[func,inputs]

_cert_cache =dict()
cert_hit = 0
def docert_cached(func, inputs):
    global cert_hit, _cert_cache
    if (func,inputs) not in _cert_cache:
        _cert_cache[func, inputs] = docert(func, inputs)
    else:
        cert_hit += 1
    return _cert_cache[func,inputs]




class FunctionDatabase(object):
    def __init__(self):
        self.smallest = dict()
        self._count = Counter()

    def addfunc(self, func, inputs):
        #type: (RBCEdge, Tuple[RBCNode, ...]) -> FunctionCertificate

        cfunc = docert(func, inputs)
        funcsize = func_size(func, inputs)
        # assert cfunc == docert(*clone_func_cached(func, inputs))
        if cfunc not in self._count:
            self.smallest[cfunc] = (funcsize,  clone_func_cached(func, inputs))
            self._count[cfunc] = 1
        else:
            self._count[cfunc] += 1
            if funcsize < self.smallest[cfunc][0]:
                self.smallest[cfunc] = (funcsize, clone_func_cached(func, inputs))
        return cfunc

    def most_common(self, n=None):
        # type: (Optional[int]) -> List[Formula]
        return [self.smallest[x[0]][1] for x in self._count.most_common(n)]


    def read_function(self, f, k = 8):
        # type: (BinaryIO, Optional[int]) -> None
        bfs = parse_aig(f) # type: Formula
        graph = build_graph(bfs)
        nset = set()
        nodemap = list(enumerate(n for n in bfs.iter_nodes(nset)))
        assert  len(nset) == graph.number_of_nodes()
        nx.relabel_nodes(graph, dict((b,a) for a, b in nodemap), copy=False)
        nodemap = dict(nodemap) # type: Dict[int, RBCNode]

        memo1, memo2 = dict(), dict()
        for out in range(len(nodemap)):
            for cut in all_cuts2(graph, out, k, memo1, memo2):
                if out in cut:
                    continue
                self.addfunc(RBCEdge(nodemap[out], False), tuple(nodemap[x] for x in cut))


class DelayedFunctionDatabase(FunctionDatabase):
    def __init__(self):
        FunctionDatabase.__init__(self)
        self.cache_clone = dict()
        self.cache_eval = dict()

    def addfunc(self, func, inputs):

        #clfunc, clinputs = clone_func_cached(func, inputs)
        #if clfunc in self.cache_clone:
        #    self._count[self.cache_clone[clfunc]] += 1
        #    return
        #func, inputs = clfunc, clinputs
        evalval = func_eval_cached(func, inputs)
        if evalval[0]:
            evalval = tuple(1-x for x in evalval)
        if evalval in self.cache_eval:
            self._count[self.cache_eval[evalval]] += 1
            return

        ckey = FunctionDatabase.addfunc(self, func, inputs)
        self.cache_eval[evalval] = ckey
        #self.cache_clone[clfunc] = ckey

    def compact(self):
        pass

    def most_common(self, n=None):
        self.compact()
        return FunctionDatabase.most_common(self,n)

    def read_function(self, f, k = 8):
        #global _clone_cache, _eval_cache
        #_clone_cache = dict()
        #_eval_cache = dict()
        return FunctionDatabase.read_function(self, f, k)
