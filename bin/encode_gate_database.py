from __future__ import print_function
from glob import glob
from os.path import dirname,basename,exists
from pyrbc.aiger import parse_aig
from gatecollector.encodeintiles import encode_aig,describe_aig
from pprint import pprint
from datetime import datetime
from fractions import  Fraction
from json import dumps

def main():
    allgates =  glob(dirname(__file__) + "/../resources/gates/g*.aig")
    genlib = open(dirname(__file__) + "/../resources/gates.genlib", "w")
    qubodir = dirname(__file__) + "/../resources/penaltyfuncs/"

    print("""GATE not 0.00 O= !I;
                PIN * INV 0 0 0 0 0 0 
                """, file=genlib)

    print ( """GATE and2 4.00 O= I0 * I1;
                    PIN * INV 0 0 0 0 0 0 
                    """, file= genlib)

    for fn in allgates:
        gatename = basename(fn)[:-4]
        outfn = qubodir + gatename + ".out"
        #skip already encoded gates



        with open(fn, "rb") as f:
            aig = parse_aig(f)

        if exists(outfn):
            print ("reading", gatename)
            with open(outfn) as f:
                model = eval(f.read())
                print (model)
        else:
            print ("searching", gatename)
            model = encode_aig(aig, "chimera")

        #log result
        print ("#[{}] model for {}({};): {}".format(datetime.now(), gatename, describe_aig(aig[0]), model))

        #save result in file
        with open(outfn, "w") as f:
            print ("# {}:".format(describe_aig(aig[0])), file=f)
            pprint(model, f)

        #print genlib gate in file
        if model:
            jsondata = {k: float(v) for k, v in model.items()}
            jsondata["filename"] = fn
            print ("""GATE {} {}.00 {};#{}
            PIN * INV 0 0 0 0 0 0 
            """.format(gatename, model["ancilla_used"] + 1 + len(aig[0].node.inputs), describe_aig(aig[0]), dumps(
                jsondata)), file=genlib)
            genlib.flush()




if __name__ == '__main__':
    main()
