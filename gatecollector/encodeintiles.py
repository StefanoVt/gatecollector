#glob dir
#for each aig
    # parse into func
    # encode minimal (how many tiles?)
    # save res in file
    # collect size info
# create db file with sizes
import networkx
from pfencoding.searchpf import search_pf_smallest
from dwave_networkx.generators.chimera import chimera_graph
from pfencoding.symmetries import  AncillaSymBreak, ChimeraSymBreak
from .rbccuts import func_size

tile_type = {
    "chimera" : (chimera_graph(1, 1, coordinates=True), [ChimeraSymBreak(1,1)]),
    "pegasus" : (networkx.complete_graph(4), [])
}

def encode_aig(aig, graphtype="chimera"):
    nx = 1 + len(aig.inputs)
    if nx > 8:
        return None


    inplist = tuple(aig.inputs.keys())
    size = func_size(aig.outputs[0], inplist)
    na = min(8-nx, (size-nx)*4 + 1)

    func = lambda xz: xz[0] == aig.eval(dict(zip(inplist, xz[1:])))[0]

    graph, arch_symbreak = tile_type[graphtype]

    res, pf = search_pf_smallest(nx, na, graph, func, extraconstr=arch_symbreak+[AncillaSymBreak()])

    return res or None

def describe_aig(aig):
    return "O = " + describe_edge(aig.outputs[0])


def describe_edge(edg, polarity=False):
    return describe_node(edg.node, polarity != edg.flip)

def describe_node(nd, polarity):
    if nd.op == "var":
        return ("!" if polarity else "")+"I" + str(nd.name)
    else:
        return ("("+describe_edge(nd.left, polarity)
                + (" + " if polarity else " * " )
                + describe_edge(nd.right, polarity) + ')')