from gatecollector.funcgen import visit_all_funcs
from pfencoding.searchpf import search_pf_smallest
from dwave_networkx.generators.chimera import chimera_graph
from itertools import product

def main():
    chimeragraph= chimera_graph(1,1)

    def encodefunc(func, size):
        nx = size + 1
        funcdict = dict(zip(product(*([(False,True)]*size)), func))
        def checkval(X):
            return X[0] == funcdict[tuple(X[1:])]
        model, _ = search_pf_smallest(nx, 8-nx, chimeragraph, checkval)
        if not model:
            return True
        else:
            print(func, model)

    visit_all_funcs(8, encodefunc)

if __name__ == '__main__':
    main()