#!/usr/bin/pypy
from glob import glob

import networkx as nx
from os.path import dirname
from pyrbc.graph import build_graph
from pyrbc.aiger import write_aig
from gatecollector import database
import sys
from datetime import datetime

from gatecollector.encodeintiles import encode_aig

sys.setrecursionlimit(100000)

def main():
    db = database.FunctionDatabase()

    fn = (dirname(__file__)+ "/../resources/c2670.aig")

    print(datetime.now(), "processing ", fn)

    with open(fn, "rb") as f:
        db.read_function(f, 5)

    print(datetime.now(), len(db.smallest))
    print()
    # save to a file the most common gates
    for i, func in enumerate(db.most_common(100)):

        print(i, func)
        #assert func.inputs == tuple(x[1] for x in sorted(func.node.inputs.items()))
        recert = database.docert(func.outputs[0], tuple(func.inputs.values()))
        assert len(func.outputs) == 1
        assert db.smallest[recert][1] ==  func
        #rerecert = database.docert(func, tuple(func.node.inputs.values()))
        #assert recert == rerecert
        print(datetime.now(), encode_aig(func, "chimera"))

if __name__ == '__main__':
    main()