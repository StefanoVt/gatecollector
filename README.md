gatecollector
=============

`gatecollector` is a Python library to collect the most common gates in a 
dataset of formulae in AND-Inverter graph form.